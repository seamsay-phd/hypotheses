==== Description

* Ensure data formats are sensible and well documented.
** File format (e.g. CSV, JSON, etc.).
** File system structure (e.g. don't hardcode paths to your Documents folder).
** Schemas can be used to help ensure documentation is up-to-date.
* Ensure data is versioned.
* Ensure your code is tested against sample data (or real data if possible).
* Ideally ensure data is expected in a form which is easily shared (e.g. don't expect somebody to set up a client-server database just to run your code).

==== What Goes Wrong When It's Absent?

* Code is harder to share (the person you're sharing with needs to spend time and energy taking the data that you've shared with them and putting it into the format your code requires).
* Analyses are no longer reproducible if the data ever changes.
** This is particularly problematic with living databases.
   If there is no row-level versioning (e.g. a `datetime_added` column) then attempting to reproduce the results is incredibly difficult.
   This in turn means that you don't know whether the original results can be trusted and therefore can't know whether new data has changed the results.
* Changes in the code can invalidate old data formats without necessarily breaking code (e.g. a column going from a raw to standardised form without a change in column name).

==== Are There Other Benefits?

* Putting effort into managing data makes it easier to apply FAIR principles.

==== How Do You Measure It?

* Checkbox criteria:
** Is data versioned?
** Is data documented?
** Is there example data?
** Does the documentation match example data?
** Does the code run correctly against example data?

==== What Are The Measurable Effects?

* Analyses should be easier to reproduce.
* Code should be easier to share leading to more papers released related to the original analysis.
* Software packages that rely on input data should be easier to get working.