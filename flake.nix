{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        nativeBuildInputs = [ pkgs.asciidoctor ];
      in {
        packages.default = let
        version = "0.0";
        in pkgs.stdenv.mkDerivation {
          pname = "hypotheses";
          inherit version ;

          src = ./.;

          inherit nativeBuildInputs;

          phases = [ "unpackPhase" "buildPhase" ];
          buildPhase = ''
            asciidoctor \
              --destination-dir $out \
              --attribute VERSION=${version} \
              Main.adoc
          '';
        };

        devShells.default = pkgs.mkShell { inherit nativeBuildInputs; };
      });
}
