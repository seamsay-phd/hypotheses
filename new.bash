#!/usr/bin/env bash

set -eu
set -o pipefail


main=Main.adoc


if [ $# -ne 1 ]
then
  1>&2 echo 'usage: '"$0"' path/to/new/file.adoc'
  exit 3
fi

if [ ! -e "$main" ]
then
  1>&2 echo 'ERROR: You must be in the directory containing the `'"$main"'` file.'
  exit 4
fi


path="$1"


if [[ "$path" = /* ]] || [[ "$path" = ../* ]] || [[ "$path" = ./* ]]
then
  1>&2 echo 'ERROR: You must use a path relative to the current directory.'
  exit 5
fi


filename_with_extension="$(basename "$path")"
filename="${filename_with_extension%.adoc}"
extension="${filename_with_extension##"$filename".}"


if [ "$extension" != adoc ]
then
  # shellcheck disable=SC2016
  1>&2 echo 'ERROR: You must use a `.adoc` extension, not `'"$extension"'`.'
  exit 6
fi

if [ -e "$path" ]
then
  1>&2 echo 'ERROR: Path `'"$path"'` already exists.'
  exit 7
fi


directory="$(dirname "$path")"


if [ -n "$directory" ] && [ ! -e "$directory" ]
then
  1>&2 echo 'INFO: Creating directory `'"$directory"'`.'
  mkdir -p "$directory"
  1>&2 echo 'WARNING: This script does not have any logic to treat files in directories as subsections!'
  1>&2 echo 'WARNING: Remember to manually add the subsections and increase the heading level of the headings in the file.'
fi


1>&2 echo 'INFO: Creating file `'"$path"'`.'
cat <<EOF >"$path"
==== Description

==== What Goes Wrong When It's Absent?

==== Are There Other Benefits?

==== How Do You Measure It?

==== What Are The Measurable Effects?
EOF


section_name="${filename//[-]/ }"

1>&2 echo 'INFO: Adding section with name `'"$section_name"'`.'
cat <<EOF >>"$main"

=== $section_name

include::${path}[]
EOF

1>&2 echo 'WARNING: Remember to order the section correctly.'